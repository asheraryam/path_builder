extends Spatial

var can_build = true

var can_x = true
var can_x_minus = true
var can_z = true
var can_z_minus = true

onready var x_ray = $x_plus
onready var x_minus_ray = $x_minus
onready var z_ray = $z_plus
onready var z_minus_ray = $z_minus

onready var cubee = preload('res://assets/cube/cubee.tscn')

var build_length = 1

const build_offset = 2

signal block_built(pos)
signal block_removed(pos)

#arrays
var rays = []
var build_ability = []

# Called when the node enters the scene tree for the first time.
func _ready():
	global.connect("length_changed", self, "_on_length_changed")
	
	rays.append(x_ray)
	rays.append(x_minus_ray)
	rays.append(z_ray)
	rays.append(z_minus_ray)
	
	build_ability.append(can_x)
	build_ability.append(can_x_minus)
	build_ability.append(can_z)
	build_ability.append(can_z_minus)
	
	check()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	check()
	if Input.is_action_just_pressed("ui_up"):
		build(0)
	if Input.is_action_just_pressed("ui_down"):
		build(1)
	if Input.is_action_just_pressed("ui_right"):
		build(2)
	if Input.is_action_just_pressed("ui_left"):
		build(3)
	if Input.is_action_just_pressed("ui_accept"):
		remove_last()	
	

func check():
	for i in rays:
		if i.is_colliding():
			build_ability[rays.find(i)] = false
			$arrows.get_child(rays.find(i)).visible = false
		
		if !i.is_colliding():
			build_ability[rays.find(i)] = true
			$arrows.get_child(rays.find(i)).visible = true
				
func build(no):
	if build_ability[no] == true:
		
		mouse_click()
		
		var new_cube = cubee.instance()
		var new_pos = self.global_transform.origin
		
		if no == 0:
			new_pos.x += build_offset
		elif no == 1:
			new_pos.x -= build_offset
		elif no == 2:
			new_pos.z += build_offset
		elif no == 3:
			new_pos.z -= build_offset
		
		#set cube stuff
		new_cube.global_transform.origin = new_pos
		
		new_cube.scale = Vector3(0,0,0)
		new_cube.grow_up()		
		
		get_parent().get_child(0).add_child(new_cube)
		
		self.global_transform.origin = new_pos
		emit_signal("block_built", new_pos)
		check()
				
	else:
		pass
		#function to play the wrong sound
		
func remove_last():
	if get_parent().get_child(0).get_child_count() > 1:
		var last_block = get_parent().get_child(0).get_children()[-1]
		var prev_pos = last_block.global_transform.origin
		last_block.queue_free()
		var new_pos = get_parent().get_child(0).get_children()[-2].global_transform.origin
		self.global_transform.origin = new_pos
		emit_signal("block_removed", new_pos)
		
func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == 4 and event.pressed:
			global.change_build_length(1)
		if event.button_index == 5 and event.pressed:
			global.change_build_length(-1)
			
func build_multiple(val):
	if can_build:
		for i in range(0, global.build_length):
			build(val)
		can_build = false
		$build_pause.start()
		
func _on_length_changed():
	mouse_over()
	for i in rays:
		#we need + 3
		var vector = i.cast_to.normalized()
		vector = vector * 2 * global.build_length
		if i.name == "x_plus":
			vector.x += 3
		if i.name == "x_minus":
			vector.x -= 3
		if i.name == "z_plus":
			vector.z += 3
		if i.name == "z_minus":
			vector.z -= 3
		i.cast_to = vector

func mouse_over():
	$mouse_over.play(0.0)
	
func mouse_click():
	$mouse_click.play(0.0)	

func _on_build_pause_timeout():
	can_build = true
